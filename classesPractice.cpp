﻿#include <iostream>
#include <cmath> // для использования sqrt и abs


class Vector
{
private:
    double a,
        b,
        c;

public:
    Vector() : a(0), b(0), c(0)
    {}

    Vector(double newA, double newB, double newC) : a(newA), b(newB), c(newC)
    {}

    void print()
    {
        std::cout << "Vector( " << a << ", " << b << ", " << c << " )\n";
    }

    double getLength()
    {
        return abs(sqrt((a * a) + (b + b) + (c * c)));
    }
};


int main()
{
    Vector vector1, vector2{1, 2, 3};

    std::cout << "\n1) ";
    vector1.print();

    std::cout << "\n2) ";
    vector2.print();

    std::cout << "\nVector 2 length is " << vector2.getLength() << '\n';
}
